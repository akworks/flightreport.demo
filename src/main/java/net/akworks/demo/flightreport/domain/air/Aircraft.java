package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.domain.DBMetadata;

import javax.persistence.*;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "aircraft")
public class Aircraft extends AbstractAirEntity implements AbstractIdEntityBuilder<Aircraft> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ac_model_id")
    private AcModel acModel;

    @Column(name = "ac_board_number")
    private String acBoardNumber;

    @Column(name = "ac_description")
    private String acDescription;

    public AcModel getAcModel() {
        return acModel;
    }

    public void setAcModel(AcModel acModel) {
        this.acModel = acModel;
    }

    public String getAcBoardNumber() {
        return acBoardNumber;
    }

    public void setAcBoardNumber(String acBoardNumber) {
        this.acBoardNumber = acBoardNumber;
    }

    public String getAcDescription() {
        return acDescription;
    }

    public void setAcDescription(String acDescription) {
        this.acDescription = acDescription;
    }
}
