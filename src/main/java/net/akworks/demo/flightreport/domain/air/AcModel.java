package net.akworks.demo.flightreport.domain.air;


import net.akworks.demo.flightreport.domain.DBMetadata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "ac_model")
public class AcModel extends AbstractAirEntity implements AbstractIdEntityBuilder<AcModel> {

    @Column(name = "manufacturer")
    private String manufacturer;

    @Column(name = "ac_model_name")
    private String acModelName;

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getAcModelName() {
        return acModelName;
    }

    public void setAcModelName(String acModelName) {
        this.acModelName = acModelName;
    }

}
