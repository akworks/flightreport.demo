package net.akworks.demo.flightreport.domain.air;


import net.akworks.demo.flightreport.domain.DBMetadata;

import javax.persistence.*;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "airport")
public class Airport extends AbstractAirEntity implements AbstractIdEntityBuilder<Airport> {

    @Column(name = "iata_code")
    private String iataCode;

    @Column(name = "airport_name")
    private String airportName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "location_id")
    private Location location;

    @Column(name = "icao_code")
    private String icaoCode;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lon")
    private Double lon;

    @Column(name = "gmt_zone")
    private String gmtZone;

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getGmtZone() {
        return gmtZone;
    }

    public void setGmtZone(String gmtZone) {
        this.gmtZone = gmtZone;
    }
}
