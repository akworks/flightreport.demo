package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.domain.DBMetadata;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "aircraft_route_schedule")
public class AircraftRouteSchedule extends AbstractAirEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aircraft_id")
    @BatchSize(size = 10)
    @NotNull
    private Aircraft aircraft;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "air_route_id")
    @BatchSize(size = 10)
    @NotNull
    private AirRoute airRoute;

    @Column(name = "schedule_dt")
    private LocalDateTime scheduleDt;

}
