package net.akworks.demo.flightreport.domain.air;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public abstract class AbstractAirEntity {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "abstractEntitySeq", schema = "air", sequenceName = "seq_global_id", allocationSize = 50)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "abstractEntitySeq")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractAirEntity that = (AbstractAirEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
