package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.domain.DBMetadata;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "flight")
@DynamicUpdate
public class Flight extends AbstractAirEntity implements AbstractIdEntityBuilder<Flight> {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "aircraft_id")
    @NotNull
    private Aircraft aircraft;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "src_airport_id")
    @NotNull
    private Airport srcAirport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dest_airport_id")
    @NotNull
    private Airport destAirport;

    @Version
    @Column(name = "version")
    private int version;

    @Column(name = "flight_nr")
    @NotNull
    private String flightNr;

    @Column(name = "src_dep_sdt")
    private LocalDateTime srcDepSdt;

    @Column(name = "src_dep_edt")
    private LocalDateTime srcDepEdt;

    @Column(name = "src_dep_adt")
    private LocalDateTime srcDepAdt;

    @Column(name = "src_dep_sdt_tz")
    private ZonedDateTime srcDepSdtTz;

    @Column(name = "src_dep_edt_tz")
    private ZonedDateTime srcDepEdtTz;

    @Column(name = "src_dep_adt_tz")
    private ZonedDateTime srcDepAdtTz;

    @Column(name = "dest_arr_sdt")
    private LocalDateTime destArrSdt;

    @Column(name = "dest_arr_edt")
    private LocalDateTime destArrEdt;

    @Column(name = "dest_arr_adt")
    private LocalDateTime destArrAdt;

    @Column(name = "dest_arr_sdt_tz")
    private ZonedDateTime destArrSdtTz;

    @Column(name = "dest_arr_edt_tz")
    private ZonedDateTime destArrEdtTz;

    @Column(name = "dest_arr_adt_tz")
    private ZonedDateTime destArrAdtTz;

    public Aircraft getAircraft() {
        return aircraft;
    }

    public void setAircraft(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    public Airport getSrcAirport() {
        return srcAirport;
    }

    public void setSrcAirport(Airport srcAirport) {
        this.srcAirport = srcAirport;
    }

    public Airport getDestAirport() {
        return destAirport;
    }

    public void setDestAirport(Airport destAirport) {
        this.destAirport = destAirport;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFlightNr() {
        return flightNr;
    }

    public void setFlightNr(String flightNr) {
        this.flightNr = flightNr;
    }

    public LocalDateTime getSrcDepSdt() {
        return srcDepSdt;
    }

    public void setSrcDepSdt(LocalDateTime srcDepSdt) {
        this.srcDepSdt = srcDepSdt;
    }

    public LocalDateTime getSrcDepEdt() {
        return srcDepEdt;
    }

    public void setSrcDepEdt(LocalDateTime srcDepEdt) {
        this.srcDepEdt = srcDepEdt;
    }

    public LocalDateTime getSrcDepAdt() {
        return srcDepAdt;
    }

    public void setSrcDepAdt(LocalDateTime srcDepAdt) {
        this.srcDepAdt = srcDepAdt;
    }

    public ZonedDateTime getSrcDepSdtTz() {
        return srcDepSdtTz;
    }

    public void setSrcDepSdtTz(ZonedDateTime srcDepSdtTz) {
        this.srcDepSdtTz = srcDepSdtTz;
    }

    public ZonedDateTime getSrcDepEdtTz() {
        return srcDepEdtTz;
    }

    public void setSrcDepEdtTz(ZonedDateTime srcDepEdtTz) {
        this.srcDepEdtTz = srcDepEdtTz;
    }

    public ZonedDateTime getSrcDepAdtTz() {
        return srcDepAdtTz;
    }

    public void setSrcDepAdtTz(ZonedDateTime srcDepAdtTz) {
        this.srcDepAdtTz = srcDepAdtTz;
    }

    public LocalDateTime getDestArrSdt() {
        return destArrSdt;
    }

    public void setDestArrSdt(LocalDateTime destArrSdt) {
        this.destArrSdt = destArrSdt;
    }

    public LocalDateTime getDestArrEdt() {
        return destArrEdt;
    }

    public void setDestArrEdt(LocalDateTime destArrEdt) {
        this.destArrEdt = destArrEdt;
    }

    public LocalDateTime getDestArrAdt() {
        return destArrAdt;
    }

    public void setDestArrAdt(LocalDateTime destArrAdt) {
        this.destArrAdt = destArrAdt;
    }

    public ZonedDateTime getDestArrSdtTz() {
        return destArrSdtTz;
    }

    public void setDestArrSdtTz(ZonedDateTime destArrSdtTz) {
        this.destArrSdtTz = destArrSdtTz;
    }

    public ZonedDateTime getDestArrEdtTz() {
        return destArrEdtTz;
    }

    public void setDestArrEdtTz(ZonedDateTime destArrEdtTz) {
        this.destArrEdtTz = destArrEdtTz;
    }

    public ZonedDateTime getDestArrAdtTz() {
        return destArrAdtTz;
    }

    public void setDestArrAdtTz(ZonedDateTime destArrAdtTz) {
        this.destArrAdtTz = destArrAdtTz;
    }
}
