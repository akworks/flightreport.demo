package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.domain.DBMetadata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "location")
public class Location extends AbstractAirEntity implements AbstractIdEntityBuilder<Location> {

    @Column(name = "location_name")
    private String locationName;

    @Column(name = "country_name")
    private String countryName;

    @Column(name = "country_code")
    private String countryCode;

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
}
