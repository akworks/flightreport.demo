package net.akworks.demo.flightreport.domain.air;


import net.akworks.demo.flightreport.domain.DBMetadata;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "air_route_details")
public class AirRouteDetails extends AbstractAirEntity implements AbstractIdEntityBuilder<AirRouteDetails> {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "src_airport_id")
    @BatchSize(size = 10)
    private Airport srcAirport;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dest_airport_id")
    @BatchSize(size = 10)
    private Airport destAirport;

    @Column(name = "order_idx")
    private Integer orderIdx;

    @Column(name = "direction")
    private String direction;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "air_route_id")
    @BatchSize(size = 10)
    private AirRoute airRoute;

    @Column(name = "flight_number")
    private String flightNumber;

    public Airport getSrcAirport() {
        return srcAirport;
    }

    public void setSrcAirport(Airport srcAirport) {
        this.srcAirport = srcAirport;
    }

    public Airport getDestAirport() {
        return destAirport;
    }

    public void setDestAirport(Airport destAirport) {
        this.destAirport = destAirport;
    }

    public Integer getOrderIdx() {
        return orderIdx;
    }

    public void setOrderIdx(Integer orderIdx) {
        this.orderIdx = orderIdx;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public AirRoute getAirRoute() {
        return airRoute;
    }

    public void setAirRoute(AirRoute airRoute) {
        this.airRoute = airRoute;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

}
