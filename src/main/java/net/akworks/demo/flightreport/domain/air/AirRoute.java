package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.domain.DBMetadata;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.List;

@Entity
@Table(schema = DBMetadata.SCHEME_AIR, name = "air_route")
public class AirRoute extends AbstractAirEntity implements AbstractIdEntityBuilder<AbstractAirEntity> {

    @Column(name = "route_name")
    private String routeName;

    @Column(name = "schedule_json")
    @Type(type = "JsonbAsString")
    private String scheduleJson;

    @OneToMany(mappedBy = "airRoute")
    @BatchSize(size = 10)
    private List<AirRouteDetails> airRouteDetails;

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getScheduleJson() {
        return scheduleJson;
    }

    public void setScheduleJson(String scheduleJson) {
        this.scheduleJson = scheduleJson;
    }

    public List<AirRouteDetails> getAirRouteDetails() {
        return airRouteDetails;
    }

    public void setAirRouteDetails(List<AirRouteDetails> airRouteDetails) {
        this.airRouteDetails = airRouteDetails;
    }
}
