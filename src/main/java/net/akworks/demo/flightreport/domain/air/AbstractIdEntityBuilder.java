package net.akworks.demo.flightreport.domain.air;

public interface AbstractIdEntityBuilder <T extends AbstractAirEntity> {

    default T id(Long id) {
        setId(id);
        return (T) this;
    }

    void setId(final Long id);

}
