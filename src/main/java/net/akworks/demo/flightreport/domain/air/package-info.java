@TypeDefs({
    @TypeDef(name = "JsonbAsString", typeClass = StringJsonUserType.class),
    @TypeDef(name = "TextArray", typeClass = StringArrayUserType.class)
})

package net.akworks.demo.flightreport.domain.air;

import net.akworks.demo.flightreport.hibernate.types.StringArrayUserType;
import net.akworks.demo.flightreport.hibernate.types.StringJsonUserType;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
