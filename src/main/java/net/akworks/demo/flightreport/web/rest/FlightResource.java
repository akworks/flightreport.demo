package net.akworks.demo.flightreport.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import net.akworks.demo.flightreport.service.FlightService;
import net.akworks.demo.flightreport.service.dto.FlightDTO;
import net.akworks.demo.flightreport.service.dto.FlightReportDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/flights")
public class FlightResource {

    private final FlightService flightService;

    public FlightResource(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("/aircraft-report")
    @Timed
    @ApiOperation("Basic report")
    public ResponseEntity<?> getReport(@RequestParam(name = "aircraftId", required = false) @ApiParam("id of aircraft") Long aircraftId) {
        List<FlightReportDTO> resultList = flightService.getReport(aircraftId);

        if (resultList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(resultList);
    }

    @GetMapping()
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> getFlights(@RequestParam("aircraftId") Long aircraftId) {

        List<FlightDTO> resultList = flightService.findFlights(aircraftId);
        if (resultList.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(resultList);
    }

    @PutMapping("")
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> putFlight(@Valid @RequestBody FlightDTO flightDTO) {

        Optional<FlightDTO> resultFlight = flightDTO.getId() == null
            ? flightService.addFlight(flightDTO)
            : flightService.updateFlight(flightDTO);

        return resultFlight.map(ResponseEntity::ok).orElse(new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE));
    }

}
