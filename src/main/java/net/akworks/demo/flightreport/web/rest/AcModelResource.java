package net.akworks.demo.flightreport.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import net.akworks.demo.flightreport.service.AcModelService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/ac-models")
public class AcModelResource {

    private final AcModelService acModelService;

    public AcModelResource(AcModelService acModelService) {
        this.acModelService = acModelService;
    }

    @GetMapping("")
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> getAcModels() {
        return ResponseEntity.ok(acModelService.findAllAcModels());
    }
}
