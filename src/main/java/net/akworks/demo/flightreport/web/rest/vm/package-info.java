/**
 * View Models used by Spring MVC REST controllers.
 */
package net.akworks.demo.flightreport.web.rest.vm;
