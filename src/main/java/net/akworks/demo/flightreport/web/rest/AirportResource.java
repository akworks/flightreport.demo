package net.akworks.demo.flightreport.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import net.akworks.demo.flightreport.service.AirportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/airports")
public class AirportResource {

    private final AirportService airportService;

    public AirportResource(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping("")
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> getAirportList() {
        return ResponseEntity.ok(airportService.findAllAirports());
    }
}
