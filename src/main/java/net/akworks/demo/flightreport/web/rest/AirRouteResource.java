package net.akworks.demo.flightreport.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import net.akworks.demo.flightreport.service.AirRouteService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/air-routes")
public class AirRouteResource {

    private final AirRouteService airRouteService;

    public AirRouteResource(AirRouteService airRouteService) {
        this.airRouteService = airRouteService;
    }

    @GetMapping("")
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> getAirRoute() {
        return ResponseEntity.ok(airRouteService.findAllRoutes());
    }
}
