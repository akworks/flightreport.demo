package net.akworks.demo.flightreport.web.rest;

import com.codahale.metrics.annotation.Timed;
import io.swagger.annotations.ApiOperation;
import net.akworks.demo.flightreport.service.AircraftService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/aircraft")
public class AircraftResource {

    private final AircraftService aircraftService;

    public AircraftResource(AircraftService aircraftService) {
        this.aircraftService = aircraftService;
    }

    @GetMapping("")
    @Timed
    @ApiOperation("")
    public ResponseEntity<?> getAircraftList() {
        return ResponseEntity.ok(aircraftService.findAllAircraft());
    }
}
