package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
}
