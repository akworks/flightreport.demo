package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.AirRoute;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirRouteRepository extends JpaRepository<AirRoute, Long> {
    List<AirRoute> findByRouteName(String routeName);
}
