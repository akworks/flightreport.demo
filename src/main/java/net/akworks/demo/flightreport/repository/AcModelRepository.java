package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.AcModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcModelRepository extends JpaRepository<AcModel, Long> {
}
