package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.Airport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirportRepository extends JpaRepository<Airport, Long> {
}
