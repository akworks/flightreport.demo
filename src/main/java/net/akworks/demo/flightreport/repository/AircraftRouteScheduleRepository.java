package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.AircraftRouteSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AircraftRouteScheduleRepository extends JpaRepository<AircraftRouteSchedule, Long> {
}
