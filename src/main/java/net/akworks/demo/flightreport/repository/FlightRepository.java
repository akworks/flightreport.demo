package net.akworks.demo.flightreport.repository;

import net.akworks.demo.flightreport.domain.air.Flight;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightRepository extends JpaRepository<Flight, Long> {

    List<Flight> findByAircraftIdOrderBySrcDepSdt(Long id);

}
