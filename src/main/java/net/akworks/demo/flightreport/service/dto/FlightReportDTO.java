package net.akworks.demo.flightreport.service.dto;

public class FlightReportDTO {

    private Long aircraftId;

    private String acBoardNumber;

    private Long airportId;

    private String airportName;

    private String airportIataCode;

    private Long landingCount;

    public Long getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(Long aircraftId) {
        this.aircraftId = aircraftId;
    }

    public String getAcBoardNumber() {
        return acBoardNumber;
    }

    public void setAcBoardNumber(String acBoardNumber) {
        this.acBoardNumber = acBoardNumber;
    }

    public Long getAirportId() {
        return airportId;
    }

    public void setAirportId(Long airportId) {
        this.airportId = airportId;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportIataCode() {
        return airportIataCode;
    }

    public void setAirportIataCode(String airportIataCode) {
        this.airportIataCode = airportIataCode;
    }

    public Long getLandingCount() {
        return landingCount;
    }

    public void setLandingCount(Long landingCount) {
        this.landingCount = landingCount;
    }
}
