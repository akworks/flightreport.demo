package net.akworks.demo.flightreport.service.dto;

public class LocationDTO {

    private Long id;

    private String locationName;

    private String countryName;

    private String countryCode;
}
