package net.akworks.demo.flightreport.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import net.akworks.demo.flightreport.service.util.JsonToStringDeserializer;

import java.util.ArrayList;
import java.util.List;

public class AirRouteDTO {

    private Long id;

    private String routeName;

    @JsonRawValue
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty(value = "schedule")
    @JsonDeserialize(using = JsonToStringDeserializer.class)
    private String scheduleJson;

    private List<AirRouteDetailsDTO> airRouteDetails = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getScheduleJson() {
        return scheduleJson;
    }

    public void setScheduleJson(String scheduleJson) {
        this.scheduleJson = scheduleJson;
    }

    public List<AirRouteDetailsDTO> getAirRouteDetails() {
        return airRouteDetails;
    }

    public void setAirRouteDetails(List<AirRouteDetailsDTO> airRouteDetails) {
        this.airRouteDetails = airRouteDetails;
    }
}
