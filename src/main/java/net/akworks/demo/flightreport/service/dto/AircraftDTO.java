package net.akworks.demo.flightreport.service.dto;

public class AircraftDTO {

    private Long id;

    private String acModelName;

    private String acBoardNumber;

    private String acDescription;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAcModelName() {
        return acModelName;
    }

    public void setAcModelName(String acModelName) {
        this.acModelName = acModelName;
    }

    public String getAcBoardNumber() {
        return acBoardNumber;
    }

    public void setAcBoardNumber(String acBoardNumber) {
        this.acBoardNumber = acBoardNumber;
    }

    public String getAcDescription() {
        return acDescription;
    }

    public void setAcDescription(String acDescription) {
        this.acDescription = acDescription;
    }
}
