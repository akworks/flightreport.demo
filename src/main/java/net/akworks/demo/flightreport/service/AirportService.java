package net.akworks.demo.flightreport.service;

import net.akworks.demo.flightreport.repository.AirportRepository;
import net.akworks.demo.flightreport.service.dto.AirportDTO;
import net.akworks.demo.flightreport.service.mapper.AirportMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AirportService {

    private final AirportRepository airportRepository;

    private final AirportMapper airportMapper;

    public AirportService(AirportRepository airportRepository, AirportMapper airportMapper) {
        this.airportRepository = airportRepository;
        this.airportMapper = airportMapper;
    }

    @Transactional(readOnly = true)
    public List<AirportDTO> findAllAirports() {
        return airportMapper.toDto(airportRepository.findAll());
    }

}
