package net.akworks.demo.flightreport.service;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.sql.PostgreSQLTemplates;
import com.querydsl.sql.SQLExpressions;
import com.querydsl.sql.SQLQuery;
import com.querydsl.sql.SQLTemplates;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class QueryDSLService {

    private static final Logger log = LoggerFactory.getLogger(QueryDSLService.class);

    public static final SQLTemplates templates = PostgreSQLTemplates.builder().printSchema().build();

    @PersistenceContext
    private EntityManager em;

    private AtomicReference<JPAQueryFactory> jpaQueryFactoryRef = new AtomicReference<>();


    @PostConstruct
    private void postInit() {
        JPAQueryFactory check = getInstance2();
        log.debug("QueryDSLService initialized: {}", check != null);
    }

    /**
     *
     * @return
     */
    private JPAQueryFactory getInstance2() {
        JPAQueryFactory result = jpaQueryFactoryRef.get();
        if (result == null) {
            if (em == null) {
                throw new IllegalStateException("EntityManager is not initialized");
            }
            synchronized (this) {
                result = new JPAQueryFactory(em);
                if (jpaQueryFactoryRef.compareAndSet(null, result)) {
                    return result;
                } else {
                    return jpaQueryFactoryRef.get();
                }
            }
        }
        return result;

    }

    /**
     *
     * @return
     */
    public JPAQueryFactory queryFactory() {
        return getInstance2();
    }

    @Transactional
    public void doWork(Work work) throws HibernateException {
        unwrapSession().doWork(work);
    }

    @Transactional
    public <T> T doReturningWork(ReturningWork<T> work) throws HibernateException {
        return unwrapSession().doReturningWork(work);
    }

    private Session unwrapSession() {
        if (em == null) {
            throw new IllegalStateException("Session Service is not available");
        }
        Session session = em.unwrap(Session.class);
        return session;
    }

    public Long getNextSequenceValue(String sequenceName) {
        return doReturningWork((c) -> {
            SQLQuery<Tuple> query = new SQLQuery<>(c, QueryDSLService.templates);
            Long nr = query.select(SQLExpressions.nextval(sequenceName)).fetchOne();
            return nr;
        });
    }

    public Long getNextSequenceValue(String schemaName, String sequenceName) {
        return getNextSequenceValue(schemaName + '.' + sequenceName);
    }

}
