package net.akworks.demo.flightreport.service.dto;

public class AcModelDTO {

    private Long id;

    private String manufacturer;

    private String acModelName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getAcModelName() {
        return acModelName;
    }

    public void setAcModelName(String acModelName) {
        this.acModelName = acModelName;
    }
}
