package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.AirRoute;
import net.akworks.demo.flightreport.service.dto.AirRouteDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AirRouteDetailsMapper.class})
public interface AirRouteMapper {

    AirRouteDTO toDto(AirRoute airRoute);

    List<AirRouteDTO> toDto(List<AirRoute> airRoute);
}
