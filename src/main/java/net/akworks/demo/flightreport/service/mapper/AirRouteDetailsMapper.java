package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.AirRouteDetails;
import net.akworks.demo.flightreport.service.dto.AirRouteDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AirRouteDetailsMapper {

    @Mapping(target = "srcAirportId", source = "srcAirport.id")
    @Mapping(target = "srcAirportIataCode", source = "srcAirport.iataCode")
    @Mapping(target = "srcAirportName", source = "srcAirport.airportName")
    @Mapping(target = "destAirportId", source = "destAirport.id")
    @Mapping(target = "destAirportIataCode", source = "destAirport.iataCode")
    @Mapping(target = "destAirportName", source = "destAirport.airportName")
    @Mapping(target = "airRouteId", source = "airRoute.id")
    AirRouteDetailsDTO toDto(AirRouteDetails airRouteDetails);

}
