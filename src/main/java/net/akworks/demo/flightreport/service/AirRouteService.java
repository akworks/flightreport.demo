package net.akworks.demo.flightreport.service;

import net.akworks.demo.flightreport.repository.AirRouteRepository;
import net.akworks.demo.flightreport.service.dto.AirRouteDTO;
import net.akworks.demo.flightreport.service.mapper.AirRouteMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AirRouteService {

    private final AirRouteRepository airRouteRepository;

    private final AirRouteMapper airRouteMapper;

    public AirRouteService(AirRouteRepository airRouteRepository, AirRouteMapper airRouteMapper) {
        this.airRouteRepository = airRouteRepository;
        this.airRouteMapper = airRouteMapper;
    }

    @Transactional(readOnly = true)
    public List<AirRouteDTO> findAllRoutes() {
        return airRouteMapper.toDto(airRouteRepository.findAll());
    }

}
