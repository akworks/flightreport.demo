package net.akworks.demo.flightreport.service.dto;

public class AirRouteDetailsDTO {

    private Long id;

    private Long srcAirportId;

    private String srcAirportIataCode;

    private String srcAirportName;

    private Long destAirportId;

    private String destAirportIataCode;

    private String destAirportName;

    private Integer orderIdx;

    private String direction;

    private Long airRouteId;

    private String flightNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSrcAirportId() {
        return srcAirportId;
    }

    public void setSrcAirportId(Long srcAirportId) {
        this.srcAirportId = srcAirportId;
    }

    public String getSrcAirportIataCode() {
        return srcAirportIataCode;
    }

    public void setSrcAirportIataCode(String srcAirportIataCode) {
        this.srcAirportIataCode = srcAirportIataCode;
    }

    public String getSrcAirportName() {
        return srcAirportName;
    }

    public void setSrcAirportName(String srcAirportName) {
        this.srcAirportName = srcAirportName;
    }

    public Long getDestAirportId() {
        return destAirportId;
    }

    public void setDestAirportId(Long destAirportId) {
        this.destAirportId = destAirportId;
    }

    public String getDestAirportIataCode() {
        return destAirportIataCode;
    }

    public void setDestAirportIataCode(String destAirportIataCode) {
        this.destAirportIataCode = destAirportIataCode;
    }

    public String getDestAirportName() {
        return destAirportName;
    }

    public void setDestAirportName(String destAirportName) {
        this.destAirportName = destAirportName;
    }

    public Integer getOrderIdx() {
        return orderIdx;
    }

    public void setOrderIdx(Integer orderIdx) {
        this.orderIdx = orderIdx;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Long getAirRouteId() {
        return airRouteId;
    }

    public void setAirRouteId(Long airRouteId) {
        this.airRouteId = airRouteId;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }
}
