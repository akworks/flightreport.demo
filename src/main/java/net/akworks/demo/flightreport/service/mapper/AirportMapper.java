package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.Airport;
import net.akworks.demo.flightreport.service.dto.AirportDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {LocationMapper.class})
public interface AirportMapper {

    AirportDTO toDto(Airport airport);

    List<AirportDTO> toDto(List<Airport> airportList);
}
