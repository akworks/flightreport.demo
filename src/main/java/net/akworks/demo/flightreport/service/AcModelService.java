package net.akworks.demo.flightreport.service;

import net.akworks.demo.flightreport.repository.AcModelRepository;
import net.akworks.demo.flightreport.service.dto.AcModelDTO;
import net.akworks.demo.flightreport.service.mapper.AcModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AcModelService {

    private final AcModelRepository acModelRepository;

    private final AcModelMapper acModelMapper;

    public AcModelService(AcModelRepository acModelRepository, AcModelMapper acModelMapper) {
        this.acModelRepository = acModelRepository;
        this.acModelMapper = acModelMapper;
    }

    @Transactional(readOnly = true)
    public List<AcModelDTO> findAllAcModels(){
        return acModelMapper.toDto(acModelRepository.findAll());
    }
}
