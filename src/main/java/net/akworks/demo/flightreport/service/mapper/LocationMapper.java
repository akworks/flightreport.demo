package net.akworks.demo.flightreport.service.mapper;


import net.akworks.demo.flightreport.domain.air.Location;
import net.akworks.demo.flightreport.service.dto.LocationDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LocationMapper {
    LocationDTO toDto(Location location);
}
