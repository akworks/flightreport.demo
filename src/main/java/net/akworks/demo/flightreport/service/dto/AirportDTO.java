package net.akworks.demo.flightreport.service.dto;

public class AirportDTO {

    private Long id;

    private String iataCode;

    private String airportName;

    private LocationDTO location;

    private String icaoCode;

    private Double lat;

    private Double lon;

    private String gmtZone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIataCode() {
        return iataCode;
    }

    public void setIataCode(String iataCode) {
        this.iataCode = iataCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public LocationDTO getLocation() {
        return location;
    }

    public void setLocation(LocationDTO location) {
        this.location = location;
    }

    public String getIcaoCode() {
        return icaoCode;
    }

    public void setIcaoCode(String icaoCode) {
        this.icaoCode = icaoCode;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getGmtZone() {
        return gmtZone;
    }

    public void setGmtZone(String gmtZone) {
        this.gmtZone = gmtZone;
    }
}
