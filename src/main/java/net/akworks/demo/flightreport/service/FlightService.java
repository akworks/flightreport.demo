package net.akworks.demo.flightreport.service;

import com.querydsl.core.Tuple;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.core.types.dsl.StringTemplate;
import com.querydsl.sql.SQLQuery;
import net.akworks.demo.flightreport.domain.air.Aircraft;
import net.akworks.demo.flightreport.domain.air.Airport;
import net.akworks.demo.flightreport.domain.air.Flight;
import net.akworks.demo.flightreport.repository.AircraftRepository;
import net.akworks.demo.flightreport.repository.AirportRepository;
import net.akworks.demo.flightreport.repository.FlightRepository;
import net.akworks.demo.flightreport.service.dto.FlightDTO;
import net.akworks.demo.flightreport.service.dto.FlightReportDTO;
import net.akworks.demo.flightreport.service.mapper.FlightMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FlightService {

    private static final Logger log = LoggerFactory.getLogger(FlightService.class);

    private final FlightRepository flightRepository;

    private final FlightMapper flightMapper;

    private final AircraftRepository aircraftRepository;

    private final AirportRepository airportRepository;

    private final QueryDSLService queryDSLService;

    public FlightService(FlightRepository flightRepository, FlightMapper flightMapper, AircraftRepository aircraftRepository, AirportRepository airportRepository, QueryDSLService queryDSLService) {
        this.flightRepository = flightRepository;
        this.flightMapper = flightMapper;
        this.aircraftRepository = aircraftRepository;
        this.airportRepository = airportRepository;
        this.queryDSLService = queryDSLService;
    }

    @Transactional(readOnly = true)
    public List<FlightReportDTO> getReport(Long airportId) {

        StringTemplate storedProcFunction = Expressions.stringTemplate("air.get_flight_report({0})",
            airportId != null ? Expressions.asNumber(airportId) : Expressions.nullExpression());

        List<FlightReportDTO> result = queryDSLService.doReturningWork((c) -> {
            SQLQuery<Tuple> query = new SQLQuery<>(c, QueryDSLService.templates)
                .select(storedProcColumns)
                .from(storedProcFunction);
            List<Tuple> resultTupleList = query.fetch();
            return resultTupleList.stream().map(this::tupleToFlightReportDTO).collect(Collectors.toList());
        });

        return result;
    }

    private final static class StoredProcResultPaths {
        private static final NumberPath<Long> aircraftId = Expressions.numberPath(Long.class, "aircraft_id");
        private static final StringPath acBoardNumber = Expressions.stringPath("ac_board_number");
        private static final NumberPath<Long> airportId = Expressions.numberPath(Long.class, "airport_id");
        private static final StringPath airportName = Expressions.stringPath("airport_name");
        private static final StringPath airportIataCode = Expressions.stringPath("airport_iata_code");
        private static final NumberPath<Long> landingCount = Expressions.numberPath(Long.class, "landing_count");
    }


    private final static Expression<?>[] storedProcColumns = Arrays.asList(
        StoredProcResultPaths.aircraftId,
        StoredProcResultPaths.acBoardNumber,
        StoredProcResultPaths.airportId,
        StoredProcResultPaths.airportName,
        StoredProcResultPaths.airportIataCode,
        StoredProcResultPaths.acBoardNumber,
        StoredProcResultPaths.landingCount
    ).toArray(new Expression<?>[0]);

    /**
     *
     * @param tuple
     * @return
     */
    private FlightReportDTO tupleToFlightReportDTO (Tuple tuple) {
        FlightReportDTO t = new FlightReportDTO();
        t.setAircraftId(tuple.get(StoredProcResultPaths.aircraftId));
        t.setAcBoardNumber(tuple.get(StoredProcResultPaths.acBoardNumber));
        t.setAirportId(tuple.get(StoredProcResultPaths.airportId));
        t.setAirportName(tuple.get(StoredProcResultPaths.airportName));
        t.setAirportIataCode(tuple.get(StoredProcResultPaths.airportIataCode));
        t.setLandingCount(tuple.get(StoredProcResultPaths.landingCount));
        return t;
    }


    @Transactional(readOnly = true)
    public List<FlightDTO> findFlights(Long aircraftId) {
        return flightMapper.toDto(flightRepository.findByAircraftIdOrderBySrcDepSdt(aircraftId));
    }


    private Optional<Flight> flightModification(Flight flight, FlightDTO flightDTO) {

        if (flightDTO.getAircraftId() == null) {
            return Optional.empty();
        }

        Aircraft aircraft = aircraftRepository.findOne(flightDTO.getAircraftId());

        if (aircraft == null) {
            return Optional.empty();
        }

        if (flightDTO.getSrcAirportId() == null) {
            return Optional.empty();
        }

        Airport srcAirport = airportRepository.findOne(flightDTO.getSrcAirportId());

        if (srcAirport == null) {
            return Optional.empty();
        }

        Airport destAirport = airportRepository.findOne(flightDTO.getDestAirportId());

        if (destAirport == null) {
            return Optional.empty();
        }

        flight.setAircraft(aircraft);
        flight.setSrcAirport(srcAirport);
        flight.setDestAirport(destAirport);
        flightMapper.updateFlight(flight, flightDTO);
        return Optional.of(flight);
    }

    @Transactional
    public Optional<FlightDTO> addFlight(FlightDTO flightDTO) {

        if (flightDTO == null || flightDTO.getId() != null) {
            return Optional.empty();
        }

        Flight flight = new Flight();
        Optional<Flight> modFlight = flightModification(flight, flightDTO);

        if (!modFlight.isPresent()) {
            return Optional.empty();
        }

        Flight resultFlight = flightRepository.save(modFlight.get());

        return Optional.ofNullable(flightMapper.toDto(resultFlight));

    }

    @Transactional
    public Optional<FlightDTO> updateFlight(FlightDTO flightDTO) {

        if (flightDTO == null || flightDTO.getId() == null) {
            return Optional.empty();
        }

        Flight flight = flightRepository.findOne(flightDTO.getId());
        if (flight == null) {
            return Optional.empty();
        }
        Optional<Flight> modFlight = flightModification(flight, flightDTO);

        Flight resultFlight = flightRepository.save(modFlight.get());
        if (!modFlight.isPresent()) {
            return Optional.empty();
        }


        return Optional.ofNullable(flightMapper.toDto(resultFlight));

    }

}
