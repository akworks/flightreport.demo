package net.akworks.demo.flightreport.service;

import net.akworks.demo.flightreport.repository.AircraftRepository;
import net.akworks.demo.flightreport.service.dto.AircraftDTO;
import net.akworks.demo.flightreport.service.mapper.AircraftMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class AircraftService {

    private final AircraftRepository aircraftRepository;

    private final AircraftMapper aircraftMapper;

    public AircraftService(AircraftRepository aircraftRepository, AircraftMapper aircraftMapper) {
        this.aircraftRepository = aircraftRepository;
        this.aircraftMapper = aircraftMapper;
    }

    @Transactional(readOnly = true)
    public List<AircraftDTO> findAllAircraft() {
        return aircraftMapper.toDto(aircraftRepository.findAll());
    }

}
