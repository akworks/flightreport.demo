package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.Aircraft;
import net.akworks.demo.flightreport.service.dto.AircraftDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AircraftMapper {

    @Mapping(target = "acModelName", source = "acModel.acModelName")
    AircraftDTO toDto(Aircraft aircraft);

    List<AircraftDTO> toDto(List<Aircraft> aircraftList);
}
