package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.Flight;
import net.akworks.demo.flightreport.service.dto.FlightDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FlightMapper {

    @Mapping(target = "aircraftId", source = "aircraft.id")
    @Mapping(target = "acBoardNumber", source = "aircraft.acBoardNumber")
    @Mapping(target = "srcAirportId", source = "srcAirport.id")
    @Mapping(target = "srcAirportIataCode", source = "srcAirport.iataCode")
    @Mapping(target = "srcAirportName", source = "srcAirport.airportName")
    @Mapping(target = "destAirportId", source = "destAirport.id")
    @Mapping(target = "destAirportIataCode", source = "destAirport.iataCode")
    @Mapping(target = "destAirportName", source = "destAirport.airportName")
    FlightDTO toDto(Flight flight);

    List<FlightDTO> toDto(List<Flight> flightList);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "aircraft", ignore = true)
    @Mapping(target = "srcAirport", ignore = true)
    @Mapping(target = "destAirport", ignore = true)
    void updateFlight(@MappingTarget Flight flight, FlightDTO flightDTO);

}
