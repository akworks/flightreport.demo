package net.akworks.demo.flightreport.service.mapper;

import net.akworks.demo.flightreport.domain.air.AcModel;
import net.akworks.demo.flightreport.service.dto.AcModelDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AcModelMapper {

    AcModelDTO toDto(AcModel acModel);

    List<AcModelDTO> toDto(List<AcModel> acModelList);

}
