package net.akworks.demo.flightreport.service.dto;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public class FlightDTO {

    private Long id;

    @NotNull
    private Long aircraftId;

    private String acBoardNumber;

    @NotNull
    private Long srcAirportId;

    private String srcAirportIataCode;

    private String srcAirportName;

    @NotNull
    private Long destAirportId;

    private String destAirportIataCode;

    private String destAirportName;

    private int version;

    @NotNull
    private String flightNr;

    private LocalDateTime srcDepSdt;

    private LocalDateTime srcDepEdt;

    private LocalDateTime srcDepAdt;

    private ZonedDateTime srcDepSdtTz;

    private ZonedDateTime srcDepEdtTz;

    private ZonedDateTime srcDepAdtTz;

    private LocalDateTime destArrSdt;

    private LocalDateTime destArrEdt;

    private LocalDateTime destArrAdt;

    private ZonedDateTime destArrSdtTz;

    private ZonedDateTime destArrEdtTz;

    private ZonedDateTime destArrAdtTz;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAircraftId() {
        return aircraftId;
    }

    public void setAircraftId(Long aircraftId) {
        this.aircraftId = aircraftId;
    }

    public String getAcBoardNumber() {
        return acBoardNumber;
    }

    public void setAcBoardNumber(String acBoardNumber) {
        this.acBoardNumber = acBoardNumber;
    }

    public Long getSrcAirportId() {
        return srcAirportId;
    }

    public void setSrcAirportId(Long srcAirportId) {
        this.srcAirportId = srcAirportId;
    }

    public String getSrcAirportIataCode() {
        return srcAirportIataCode;
    }

    public void setSrcAirportIataCode(String srcAirportIataCode) {
        this.srcAirportIataCode = srcAirportIataCode;
    }

    public String getSrcAirportName() {
        return srcAirportName;
    }

    public void setSrcAirportName(String srcAirportName) {
        this.srcAirportName = srcAirportName;
    }

    public Long getDestAirportId() {
        return destAirportId;
    }

    public void setDestAirportId(Long destAirportId) {
        this.destAirportId = destAirportId;
    }

    public String getDestAirportIataCode() {
        return destAirportIataCode;
    }

    public void setDestAirportIataCode(String destAirportIataCode) {
        this.destAirportIataCode = destAirportIataCode;
    }

    public String getDestAirportName() {
        return destAirportName;
    }

    public void setDestAirportName(String destAirportName) {
        this.destAirportName = destAirportName;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFlightNr() {
        return flightNr;
    }

    public void setFlightNr(String flightNr) {
        this.flightNr = flightNr;
    }

    public LocalDateTime getSrcDepSdt() {
        return srcDepSdt;
    }

    public void setSrcDepSdt(LocalDateTime srcDepSdt) {
        this.srcDepSdt = srcDepSdt;
    }

    public LocalDateTime getSrcDepEdt() {
        return srcDepEdt;
    }

    public void setSrcDepEdt(LocalDateTime srcDepEdt) {
        this.srcDepEdt = srcDepEdt;
    }

    public LocalDateTime getSrcDepAdt() {
        return srcDepAdt;
    }

    public void setSrcDepAdt(LocalDateTime srcDepAdt) {
        this.srcDepAdt = srcDepAdt;
    }

    public ZonedDateTime getSrcDepSdtTz() {
        return srcDepSdtTz;
    }

    public void setSrcDepSdtTz(ZonedDateTime srcDepSdtTz) {
        this.srcDepSdtTz = srcDepSdtTz;
    }

    public ZonedDateTime getSrcDepEdtTz() {
        return srcDepEdtTz;
    }

    public void setSrcDepEdtTz(ZonedDateTime srcDepEdtTz) {
        this.srcDepEdtTz = srcDepEdtTz;
    }

    public ZonedDateTime getSrcDepAdtTz() {
        return srcDepAdtTz;
    }

    public void setSrcDepAdtTz(ZonedDateTime srcDepAdtTz) {
        this.srcDepAdtTz = srcDepAdtTz;
    }

    public LocalDateTime getDestArrSdt() {
        return destArrSdt;
    }

    public void setDestArrSdt(LocalDateTime destArrSdt) {
        this.destArrSdt = destArrSdt;
    }

    public LocalDateTime getDestArrEdt() {
        return destArrEdt;
    }

    public void setDestArrEdt(LocalDateTime destArrEdt) {
        this.destArrEdt = destArrEdt;
    }

    public LocalDateTime getDestArrAdt() {
        return destArrAdt;
    }

    public void setDestArrAdt(LocalDateTime destArrAdt) {
        this.destArrAdt = destArrAdt;
    }

    public ZonedDateTime getDestArrSdtTz() {
        return destArrSdtTz;
    }

    public void setDestArrSdtTz(ZonedDateTime destArrSdtTz) {
        this.destArrSdtTz = destArrSdtTz;
    }

    public ZonedDateTime getDestArrEdtTz() {
        return destArrEdtTz;
    }

    public void setDestArrEdtTz(ZonedDateTime destArrEdtTz) {
        this.destArrEdtTz = destArrEdtTz;
    }

    public ZonedDateTime getDestArrAdtTz() {
        return destArrAdtTz;
    }

    public void setDestArrAdtTz(ZonedDateTime destArrAdtTz) {
        this.destArrAdtTz = destArrAdtTz;
    }
}
