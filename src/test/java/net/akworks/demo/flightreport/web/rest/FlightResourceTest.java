package net.akworks.demo.flightreport.web.rest;

import net.akworks.demo.flightreport.FlightReportApp;
import net.akworks.demo.flightreport.domain.air.*;
import net.akworks.demo.flightreport.repository.AcModelRepository;
import net.akworks.demo.flightreport.repository.AirRouteRepository;
import net.akworks.demo.flightreport.repository.AircraftRepository;
import net.akworks.demo.flightreport.repository.FlightRepository;
import net.akworks.demo.flightreport.service.FlightService;
import net.akworks.demo.flightreport.service.mapper.AcModelMapper;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FlightReportApp.class)
@Transactional
public class FlightResourceTest {

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restMockMvc;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private FlightService flightService;

    private FlightResource flightResource;

    @Autowired
    private AcModelRepository acModelRepository;

    @Autowired
    private AircraftRepository aircraftRepository;

    @Autowired
    private AirRouteRepository airRouteRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        flightResource = new FlightResource(flightService);

        this.restMockMvc = MockMvcBuilders.standaloneSetup(flightResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    public static final String TEST_AC_BOARD_NUMBER = "TEST-777";

    private AcModel addAcModel() {
        AcModel acModel = new AcModel();
        acModel.setManufacturer("Airbus");
        acModel.setAcModelName("Airbus A300-TEST");
        return acModelRepository.saveAndFlush(acModel);
    }

    private Aircraft addAircraft(String acBoardNumber) {
        Aircraft aircraft = new Aircraft();
        aircraft.setAcModel(addAcModel());
        aircraft.setAcBoardNumber(acBoardNumber);
        aircraft.setAcDescription("Testing");
        return aircraftRepository.saveAndFlush(aircraft);
    }


    private void addFlight(AirRoute airRoute, Aircraft aircraft) {
        airRoute.getAirRouteDetails()
            .sort(Comparator.comparing(AirRouteDetails::getOrderIdx,
                Comparator.nullsLast(Comparator.naturalOrder())));

        ZonedDateTime flightTime = ZonedDateTime.now();
        int flightNr = 1;

        for (AirRouteDetails rd : airRoute.getAirRouteDetails()) {
            Flight flight = new Flight();
            flight.setAircraft(aircraft);
            flight.setSrcAirport(rd.getSrcAirport());
            flight.setDestAirport(rd.getDestAirport());

            flight.setSrcDepSdt(flightTime.toLocalDateTime());
            flight.setSrcDepEdt(flightTime.toLocalDateTime());
            flight.setSrcDepAdt(flightTime.toLocalDateTime());

            flight.setSrcDepSdtTz(flightTime);
            flight.setSrcDepEdtTz(flightTime);
            flight.setSrcDepAdtTz(flightTime);

            flightTime = flightTime.plusHours(2);

            flight.setDestArrSdt(flightTime.toLocalDateTime());
            flight.setDestArrEdt(flightTime.toLocalDateTime());
            flight.setDestArrAdt(flightTime.toLocalDateTime());

            flight.setDestArrSdtTz(flightTime);
            flight.setDestArrEdtTz(flightTime);
            flight.setDestArrAdtTz(flightTime);

            flight.setFlightNr(String.valueOf(flightNr));

            flightRepository.saveAndFlush(flight);

        }
    }


    @Test
    public void getReport() throws Exception {

        Aircraft aircraft = addAircraft(TEST_AC_BOARD_NUMBER);

        List<AirRoute> airRoute = airRouteRepository.findByRouteName("M1");
        assertThat(airRoute, Matchers.hasSize(1));

        addFlight(airRoute.get(0), aircraft);
        addFlight(airRoute.get(0), aircraft);


        restMockMvc.perform(get("/api/v1/flights/aircraft-report")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(jsonPath("$.[*].aircraftId").value(hasItem(aircraft.getId().intValue())))
            .andExpect(jsonPath("$.[*].landingCount").value(hasItem(2)));
    }

    @Test
    public void getFlight() throws Exception {

        Aircraft aircraft = addAircraft(TEST_AC_BOARD_NUMBER);

        List<AirRoute> airRoute = airRouteRepository.findByRouteName("M1");
        assertThat(airRoute, Matchers.hasSize(1));

        addFlight(airRoute.get(0), aircraft);

        restMockMvc.perform(get("/api/v1/flights")
            .param("aircraftId", aircraft.getId().toString()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(jsonPath("$.[*].aircraftId").value(hasItem(aircraft.getId().intValue())));

    }
}
