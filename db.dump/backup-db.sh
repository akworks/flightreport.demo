#!/bin/bash
DATE=`date +"%Y%m%d-%H:%M:%S"`
pg_dump -h 127.0.0.1 -p 5434 flightReport -U flightReport -w > ./backups/flight-report-${DATE}.backup
